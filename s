using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;



public class Arkanoid : EditorWindow
{
    //PTData DB;

    [MenuItem("NEXTI/Ark!")]
    static void ToolBarWindow()
    {
        Arkanoid window = (Arkanoid)EditorWindow.GetWindow(typeof(Arkanoid));
        window.Show();
    }

    GUIStyle Ball = new GUIStyle();
    float speed;
    float xdir;
    float ydir;
    float ballXpos;
    float ballYpos;

    float PaddlePosX;
    float PaddlePosY;
    float paddleWidth;

    int[,] stageBricks;

    void OnEnable()
    {
        //LoadData();  //데이터 로드
        stageBricks = new int[2,5];
       

        speed = 0.3f;
        xdir = 0.3f;
        ydir = 0.3f;
        ballXpos = 0;
        ballYpos = 100;
        PaddlePosY = Screen.height - 80;
        paddleWidth = 50;
        BrickCreate();

    }

    void BrickCreate()
    {
        for(int i = 0; i<2; i++)
        {
            for(int j=0; j < 5; j++)
            {
                stageBricks[i, j] = 1;
            }
        }
    }

    //void LoadData()
    //{
    //    DB = (PTData)AssetDatabase.LoadAssetAtPath("Assets/POWERTOOLS/Data/ptDatas.asset", typeof(PTData));
    //}


    Vector2 pos = new Vector2(0, 0);
    Vector2 scale = new Vector2(Screen.width / 5, 15);

    void OnGUI()
    {

        DrawBricks();
        Balls();
        Paddle();

        Repaint();
    }

    void DrawBricks()
    {
        for(int i=0; i < 2; i++)
        {
            for(int j = 0; j<5; j++)
            {
                
                if (stageBricks[i,j] == 1)
                {
                    pos.x = (scale.x * j);
                    GUI.Box(new Rect(pos, scale), "");
                    
                }
                else
                {
                    pos.x = Screen.width + 100;
                    //GUI.Box(new Rect(pos, scale), "");
                }
                if((ballXpos > pos.x && ballXpos < pos.x+scale.x) && (ballYpos > pos.y && ballYpos <pos.y +scale.y))
                {
                    xdir *= -1;
                    ydir *= -1;
                    stageBricks[i, j] = 0;
                    
                   
                }
            }
            pos.y = (scale.y * i);
        }
    }
    

    void Balls()
    {
        ballXpos += xdir;
        ballYpos += ydir;
        if (ballXpos > Screen.width - 10) xdir *= -1;
        if (ballXpos < 0) xdir *= -1;
        //if (ypos > Screen.height - 30)
        if (ballYpos > Screen.height - 85 && (ballXpos > PaddlePosX-25 && ballXpos < PaddlePosX+paddleWidth)) ydir *= -1;
        if (ballYpos < 0) ydir *= -1;
        GUI.Box(new Rect(ballXpos, ballYpos, 10, 10), "");
    }



    void Paddle()
    {

        GUI.Box(new Rect(PaddlePosX-25, Screen.height - 80, paddleWidth, 20), "");
        PaddlePosX = GUI.HorizontalSlider(new Rect(0, Screen.height - 50, Screen.width, 20), PaddlePosX, 0, Screen.width);

    }


}
